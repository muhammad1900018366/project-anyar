package Dilanisme21;

import java.util.Scanner;

public class Vit {
    public void vit(int x){
        int harga = 8700;
        Scanner masuk = new Scanner(System.in);
        int pil;
        System.out.println("Pembayaran bisa lewat Tunai atau E-Wallet(GoPay)");
        System.out.println("Ketik 1 untuk Tunai atau Ketik 2 untuk E-Wallet(GoPay)");
        System.out.print("Pilihan: ");
        pil = masuk.nextInt();
        switch (pil){
            case 1:
                System.out.println("Total yang Anda Bayarkan: " + x*harga);
                System.out.println("Silahkan Bayar pada Kurir Kami. Kami Antarkan sesuai Alamat Anda. Terima Kasih.");
                break;
            case 2:
                System.out.println("Total yang Anda Bayarkan: " + x*harga);
                System.out.println("Sudah terpotong dengan GoPay Anda. Kami Antarkan sesuai Alamat Anda. Terima Kasih.");
                break;
            default:
                System.out.println("Salah Inputan.");
        }
    }
}
